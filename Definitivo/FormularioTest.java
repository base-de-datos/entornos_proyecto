import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * The test class FormularioTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class FormularioTest
{
    JTextField text1,text2,text3,text4,text5,text6,text7,text8,text9,text10,text11,text12,text13;
    Formulario formulario;

    /**
     * Default constructor for test class FormularioTest
     */
    public FormularioTest()
    {
        
    }
public void a()
{
      text1 =new JTextField();
        text2=new JTextField();
        text3 =new JTextField();
        text4 =new JTextField();
        text5 =new JTextField();
        text6 = new JTextField();
        text7 = new JTextField();
        text8 = new JTextField();
        text9 = new JTextField();
        text10 = new JTextField();
        text11 = new JTextField();
        text12 = new JTextField();
        text13 = new JTextField();
}  


  /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    } 
    
    @Test
    public void prueba1() //campos vacios
    { formulario = new Formulario();
        if(text2.getText().length()==0){
     System.out.println("No hay texto");
    }
    else{
     System.out.println("Si hay texto");
    }
    }
    
    public void prueba2() //numeor no más de 6 de longitud
    { formulario = new Formulario();
        if(text6.getText().length()>9){
     System.out.println("No es Correcto");
    }
    else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba3() //nºasignaturas respecto a alumnos
    { formulario = new Formulario();
        if(text1.getText()== "8" && text8.getText()=="Alumno"){
     System.out.println("Es correcto");
    }else if(text1.getText().length()<8 && text8.getText()=="Alumno")
        {
             System.out.println("Tiene que tener mas asignaturas");
    } else if(text1.getText().length()>8 && text8.getText()=="Alumno")
    {
         System.out.println("Tiene que tener menos asignaturas");
    }else{
        System.out.println("No es correcto");
    }
    }
    
    public void prueba4() //nºasignaturas respecto profesores
    { formulario = new Formulario();
        if(text1.getText()== "10" && text8.getText()=="Profesor"){
     System.out.println("Es correcto");
    }else if(text1.getText().length()<10 && text8.getText()=="Profesor")
        {
             System.out.println("Tiene que tener mas asignaturas");
    } else if(text1.getText().length()>10 && text8.getText()=="Profesor")
    {
         System.out.println("Tiene que tener menos asignaturas");
    }else{
     System.out.println("No es correcto");
    }
    }
    
    public void prueba5() //No dos DNI iguales
    { formulario = new Formulario();
        if(text7.getText()==text7.getText()){
     System.out.println("No puede haber dos DNi iguales");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba6() //ocupacion solo puede ser profesor, alumno o directivo
    { formulario = new Formulario();
        if(text8.getText()=="Profesor" || text8.getText()=="Alumno" || text8.getText()=="Directivo"){
     System.out.println("Es correcto correcto");
    }else{
     System.out.println("No correcto");
    }
    }
    
    public void prueba7() //Mismo nombre, primer apellido y segundo apellido
    { formulario = new Formulario();
        if(text3.getText()==text3.getText() && text4.getText()==text4.getText() && text5.getText()==text5.getText() ){
     System.out.println("No puede haber la misma persona");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba8() //nº horas totales respecto profesor
    { formulario = new Formulario();
        if(text8.getText()=="Profesor" && text13.getText().length()>30 ){
     System.out.println("No puede tener más de 30 horas");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba9() //nº horas totales respecto profesor
    { formulario = new Formulario();
        if(text8.getText()=="Profesor" && text13.getText().length()<30){
     System.out.println("No puede tener menos de 30 horas");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueb10() //nº horas totales respecto alumnos
    { formulario = new Formulario();
        if(text8.getText()=="Alumno" && text13.getText().length()>30 ){
     System.out.println("No puede tener más de 30 horas");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba11() //nº horas totales respecto alumno
    { formulario = new Formulario();
        if(text8.getText()=="Alumno" && text13.getText().length()<30 ){
     System.out.println("No puede tener más de 30 horas");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba12() //nº horas totales respecto directivo
    { formulario = new Formulario();
        if(text8.getText()=="Directivo" && text13.getText().length()>8 ){
     System.out.println("No puede tener más de 8 horas");
    }else{
     System.out.println("Es correcto");
    }
    }
    
    public void prueba13() //nº horas totales respecto directivo
    { formulario = new Formulario();
        if(text8.getText()=="Directivo" && text13.getText().length()<8 ){
     System.out.println("No puede tener menos de 8 horas");
    }else{
     System.out.println("Es correcto");
    }
    }
   
    public void prueba14() //no puede haber dos correos iguales
    { formulario = new Formulario();
        if(text11.getText()==text11.getText()){
     System.out.println("No correcto");
    }else{
     System.out.println("Es correcto");
    }
    }
   
    public void prueba15() //no puede haber dos codigos iguales
    { formulario = new Formulario();
        if(text2.getText()==text2.getText()){
     System.out.println("No correcto");
    }else{
     System.out.println("Es correcto");
    }
    } 
      
    public void prueba16() //no puede haber dos telefonos iguales
    { formulario = new Formulario();
        if(text6.getText()==text6.getText()){
     System.out.println("No correcto");
    }else{
     System.out.println("Es correcto");
    }
    } 
    
    public static void main(String[] args) {
 
        Formulario pa = new Formulario();
        FormularioTest test = new FormularioTest();
        
 
    }
}
