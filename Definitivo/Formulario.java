
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.ActionListener;

public class Formulario {
    JFrame frame;
    JPanel paneliz,panelde,panelab,panelar,panelex;
    JLabel num_asignaturas,nombre,apellido1,apellido2,telefono,dni,ocupacion,pais,Codigo,fecha_nacimiento,correo,asignaturas,horas_totales;
    JTextField text1,text2,text3,text4,text5,text6,text7,text8,text9,text10,text11,text12,text13;
    JButton boton1,boton2;
          
    public void PanelBD(){
         
        frame = new JFrame();
        paneliz  = new JPanel();
        panelde  = new JPanel();
        panelab  = new JPanel();
        panelar  = new JPanel();
        panelex = new JPanel();
        // ------------------------------
        Codigo = new JLabel();
        nombre = new JLabel();
        apellido1 = new JLabel();
        apellido2 = new JLabel();
        telefono = new JLabel();
        dni = new JLabel();
        ocupacion = new JLabel();
        pais = new JLabel();
        fecha_nacimiento = new JLabel();
        correo = new JLabel();
        asignaturas = new JLabel();
        horas_totales = new JLabel();
        num_asignaturas = new JLabel();
        
        // ------------------------------
        text1 = new JTextField();
        text2 = new JTextField();
        text3 = new JTextField();
        text4 = new JTextField();
        text5 = new JTextField();
        text6 = new JTextField();
        text7 = new JTextField();
        text8 = new JTextField();
        text9 = new JTextField();
        text10 = new JTextField();
        text11 = new JTextField();
        text12 = new JTextField();
        text13 = new JTextField();
        
        // ------------------------------
        boton1 = new JButton();
        boton2 = new JButton();
         
        frame.setTitle("Formulario");
        frame.setLayout(new FlowLayout());      
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setSize(1000,1000);
        
        ocupacion.setText("Ocupacion / Cargo");
        nombre.setText("Nombre");
        apellido1.setText("Apellido1");
        apellido2.setText("Apellido2");
        dni.setText("DNI");
        fecha_nacimiento.setText("Fecha de Nacimiento");
        telefono.setText("Telefono");
        pais.setText("Pais");
        correo.setText("Correo electrónico");
        Codigo.setText("Codigo: Alumno/Profesor");
        num_asignaturas.setText("Numero de Asignaturas");
        asignaturas.setText("Asignaturas");
        horas_totales.setText("Horas Totales");
        
        
        // -------------------------------
        boton1.setText("Aceptar");
        boton2.setText("Cancelar");
                 
        paneliz.setLayout(new GridLayout(13,0));
        paneliz.add(Codigo);
        paneliz.add(apellido1);
        paneliz.add(apellido2);
        paneliz.add(nombre);
        paneliz.add(telefono);
        paneliz.add(dni);
        paneliz.add(ocupacion);
        paneliz.add(pais);
        paneliz.add(fecha_nacimiento);
        paneliz.add(correo);
        paneliz.add(num_asignaturas);
        paneliz.add(asignaturas);
        paneliz.add(horas_totales);
        
        
        panelde.setLayout(new GridLayout(13,0));
        panelde.add(text1);
        panelde.add(text2);
        panelde.add(text3);
        panelde.add(text4);
        panelde.add(text5);
        panelde.add(text6);
        panelde.add(text7);
        panelde.add(text8);
        panelde.add(text9);
        panelde.add(text10);
        panelde.add(text11);
        panelde.add(text12);
        panelde.add(text13);
        
        panelar.setLayout(new GridLayout(1,1));
        panelar.setPreferredSize(new Dimension(300,400));
        panelar.add(paneliz);
        panelar.add(panelde);
                 
        panelab.setLayout(new FlowLayout());
        panelab.setPreferredSize(new Dimension(200,100));
        panelab.add(boton1);
        panelab.add(boton2);
         
        panelex.setLayout(new GridLayout(2,0));
        panelex.add(panelar);
        panelex.add(panelab);
         
        frame.add(panelex);
      
        // acciones con los botones
        boton1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent btn1){
                JOptionPane.showMessageDialog(null,"Usuario añadido a la lista");
                System.out.print("Alumno: ");
                System.out.println(text4.getText());
                
            }
        });
        
        boton2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent btn2){
             System.exit(0);   
            }
        });
    }
     
    public static void main(String[] args) {
 
        Formulario pa = new Formulario();
        
 
    }
    
}